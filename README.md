# aoc-toolbox

## 📖 About

**aoc-toolbox** is a Rust library designed to simplify your [Advent of Code](https://adventofcode.com/) experience. With this toolbox, you can:

- Mark your solvers with minimal boilerplate.
- Automatically generate a main function with argument parsing.

## 📦 Installation

Add the following dependency:

```bash
cargo add aoc-toolbox
```

## 🚀 Usage

### 1. Mark Your Solvers

Use the `#[aoc_solver]` macro to annotate your solver functions. For example:

```rust
use aoc_toolbox::aoc_solver;

#[aoc_solver("day01", "part1")]
fn solve_part1(input: String) -> String {
  // Your solution logic here
}

#[aoc_solver("day01", "part2", "result")]
fn solve_part1(input: String) -> String {
  // Your solution logic here
}
```

If you provide a third argument to the `aoc_solver` macro, it will be tested against the output of the solver. This is useful for validating your solutions.
In that case, the output will be green or red depending on whether the result matches the expected value.

### 2. Generate the Main Function

Use the `aoc_main!` macro in your `main.rs` file to tie everything together:

```rust
use aoc_toolbox::aoc_main;

mod day01;

aoc_main! { 2024 }
```

### 3. Provide Input Files

You must provide a `.txt` file as input for each day, located at: `input/day01.txt`. The solver will automatically load the appropriate file based on the selected day.

### How It Works

The `aoc_main!` macro will generate a main function that:

- Parses the command-line arguments.
- Runs the specified solver for the selected day and part.

### Example Command

```bash
cargo run -- --help
cargo run -- --list
cargo run -- day01 # run all solvers for day 1
cargo run -- day01::part1 # run a specific solver
```

## ⚠️ Limitations

- **Macro State**: Rust macros do not currently share state between calls (see [this issue](https://github.com/rust-lang/rust/issues/44034)). A workaround is implemented, but it may lead to unexpected behavior.
- **Solver Signature**: Each solver function must take a `String` as input and also return a `String`.

## 💡 Motivation

This tool was initially created for personal use during Advent of Code, but it's now available for anyone who finds it helpful. Enjoy hacking away at those puzzles! 🎄✨

## Update

To create a new version, use `cargo-workspaces`:

```bash
cargo ws version
```

Then pipeline will publish the new version to crates.io.

