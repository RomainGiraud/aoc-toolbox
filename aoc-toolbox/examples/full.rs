use aoc_toolbox::{aoc_main, aoc_solver};

#[aoc_solver("day01", "part1")]
fn solve_day01_part1(_input: String) -> String {
    format!("{}", 32u64)
}

mod inner {
    use super::*;

    #[aoc_solver("day01", "part2")]
    pub fn solve_day01_part2(_input: String) -> String {
        "valid".to_string()
    }
}

aoc_main! { 2020 }
